package com.ltmonitor.dao.impl;

import java.io.Serializable;

import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import com.ltmonitor.dao.IBaseDao;
import com.ltmonitor.entity.TenantEntity;


public class DaoHibernateImpl extends HibernateDaoSupport implements IBaseDao {

	private static Logger log = Logger.getLogger(DaoHibernateImpl.class);

	public Serializable save(Object obj) {
		HibernateTemplate template = getHibernateTemplate();
		return template.save(obj);
	}

	public void update(Object obj) {
		HibernateTemplate template = getHibernateTemplate();
		template.update(obj);
	}

	public void saveOrUpdate(Object obj) {
		HibernateTemplate template = getHibernateTemplate();
		template.saveOrUpdate(obj);
	}

	public void remove(Object obj) {
		HibernateTemplate template = getHibernateTemplate();
		template.delete(obj);

	}

	/**
	 * 根据类名和ID来删除对�?
	 */
	public void remove(Class clazz, Serializable id) {
		Object obj = load(clazz, id);

		remove(obj);
	}

	/**
	 * 假删除对�?
	 */
	public void removeByFake(TenantEntity obj) {
		obj.setDeleted(true);


		HibernateTemplate template = getHibernateTemplate();
		template.saveOrUpdate(obj);
	}

	public void removeByFake(Class clazz, Serializable id) {
		HibernateTemplate template = getHibernateTemplate();
		TenantEntity obj = (TenantEntity) template.load(clazz, id);
		obj.setDeleted(true);
		template.saveOrUpdate(obj);
	}

	public List loadAll(Class clazz) {
		HibernateTemplate template = getHibernateTemplate();
		return template.loadAll(clazz);
	}

	/**
	 * 批量保存
	 * 
	 * @param entities
	 */
	public void saveOrUpdateAll(Collection entities) {
		super.getHibernateTemplate().saveOrUpdateAll(entities);
	}

	/**
	 * 命名查询
	 * 
	 * @param queryID
	 * @param params
	 * @return
	 */
	public List queryByNamedQuery(String queryID, Object[] params) {

		return super.getHibernateTemplate().findByNamedQuery(queryID, params);

	}
	
	

	/**
	 * 加载对象时，同时加锁
	 * 
	 * @param aclass
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public Object findAndLockById(Class aclass, String id) throws Exception {
		return super.getHibernateTemplate().get(aclass, id, LockMode.UPGRADE);
	}

	public Object load(Class clazz, Serializable sid) {
		HibernateTemplate template = getHibernateTemplate();
		return template.load(clazz, sid);
	}

	public Session getCurrentSession() {
		return getSession();
	}


	public Object find(String hsql) {
		List results = query(hsql);
		if (results != null && results.size() > 0) {
			return results.get(0);
		}
		return null;
	}

	public Object find(String hsql, Object value) {
		List results = getHibernateTemplate().find(hsql, value);
		if (results != null && results.size() > 0) {
			return results.get(0);
		}
		return null;
	}

	public Object find(String hsql, Object[] values) {
		List results = getHibernateTemplate().find(hsql, values);
		if (results != null && results.size() > 0) {
			return results.get(0);
		}
		return null;
	}
	

	public List query(final String hql) {
		return getHibernateTemplate().find(hql);
	}
	

	public List queryByNamedParam(final String hql, String paramName, final Object value) {
		return getHibernateTemplate().findByNamedParam(hql, paramName, value);
	}

	public List query(final String hql, final Object[] values) {
		return getHibernateTemplate().find(hql, values);
		
	}
	
	public List query(final String hql, final Object value) {
		return getHibernateTemplate().find(hql, value);
		
	}
	

	public List pageQuery(DetachedCriteria detachedCriteria, int firstResult, int maxResults) {
		// DetachCriteria criteria = DetachCriteria.
		return getHibernateTemplate()
				.findByCriteria(detachedCriteria, firstResult, maxResults);
	}


}